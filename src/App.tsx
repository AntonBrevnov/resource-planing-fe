import { useAppSelector } from 'hooks/useAppSelector';
import { BrowserRouter } from 'react-router-dom';
import { AuthorizedRoutes, UnauthorizedRoutes } from 'router';
import { selectors } from 'store/ducks';

function App() {
  const isAuth = useAppSelector(selectors.auth.selectToken);

  return <BrowserRouter>{isAuth ? <AuthorizedRoutes /> : <UnauthorizedRoutes />}</BrowserRouter>;
}

export default App;
