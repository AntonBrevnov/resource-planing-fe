export const colors = {
  primary: {
    600: '#6663FF',
    500: '#9390FF',
    400: '#BFBEFF',
    300: '#DEDDFF',
    100: '#F2F2FF',
    0: '#F7F7FF',
  },
  secondary: {
    blue: '#3664D9',
    lightPurple: '#F1F1FE',
  },
  grayscale: {
    90: '#4C4949',
    80: '#959595',
    70: '#ADADAD',
    60: '#BFBFBF',
    50: '#BFBFBF',
    40: '#EEEEEE',
    30: '#E3E3E3',
    20: '#F1F1F1',
    10: '#FBFBFB',
    0: '#F3F3F3',
    white: '#FFFFFF',
    black: '#3F3232',
  },
  system: {
    mistake: '#ED2929',
    warning: '#FFBD5A',
    warningHover: '#FFA929',
    warningPressed: '#F59300',
  },
  additional: {
    red: '#FFB9B9',
    yellow: '#FFDB95',
  },
};
