export const typography = {
  title_16_semi_bold: {
    fonWeight: 600,
    fontSize: '16px',
    linHeight: '24px',
  },
  title_16_regular: {
    fonWeight: 400,
    fontSize: '16px',
    linHeight: '24px',
  },
  text_14_bold: {
    fonWeight: 500,
    fontSize: '14px',
    linHeight: '16px',
  },
  text_14_regular: {
    fonWeight: 400,
    fontSize: '14px',
    linHeight: '18px',
  },
  text_small_12_regular: {
    fonWeight: 400,
    fontSize: '12px',
    linHeight: '16px',
  },
  text_very_small_regular: {
    fonWeight: 400,
    fontSize: '11px',
    linHeight: '14px',
  },
  text_very_small_bold: {
    fonWeight: 500,
    fontSize: '11px',
    linHeight: '14px',
  },
};
