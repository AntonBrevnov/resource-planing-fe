import { AnyAction, Dispatch } from 'redux';
import { REHYDRATE } from 'redux-persist';
import { http } from 'services/http';
import { thunks } from 'store/ducks';

export const authMiddleware =
  () =>
  (next: Dispatch) =>
  (action: AnyAction): AnyAction => {
    if (action.type === thunks.auth.signInUserAsyncAction.fulfilled.type) {
      action.payload && http.setAuthorizationHeader(action.payload);
    }

    if (action.type === REHYDRATE && action.payload) {
      action.payload.auth?.accessToken && http.setAuthorizationHeader(action.payload.auth?.accessToken);
    }

    return next(action);
  };
