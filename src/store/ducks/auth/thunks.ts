import { createAsyncThunk } from '@reduxjs/toolkit';
import { SignInUserDto } from 'api/generated';
import { http } from 'services/http';

export const signInUserAsyncAction = createAsyncThunk<string | null, SignInUserDto>('USER/AUTH', async (payload) => {
  try {
    const { data } = await http.post('/users/login', payload);
    return data.access_token;
  } catch (e) {
    return null;
  }
});
