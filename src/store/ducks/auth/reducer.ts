import { createReducer } from '@reduxjs/toolkit';

import { signInUserAsyncAction } from './thunks';

type AuthReducerType = {
  token: string | null;
};

const initialState: AuthReducerType = {
  token: null,
};

export const authReducer = createReducer(initialState, (builder) => {
  builder.addCase(signInUserAsyncAction.fulfilled, (state, { payload }) => {
    state.token = payload;
  });
});
