import { combineReducers } from '@reduxjs/toolkit';

import { authReducer, authSelectors, authThunks } from './auth';

export const reducer = combineReducers({
  auth: authReducer,
});

export const thunks = {
  auth: authThunks,
};

export const actions = {};

export const selectors = {
  auth: authSelectors,
};
