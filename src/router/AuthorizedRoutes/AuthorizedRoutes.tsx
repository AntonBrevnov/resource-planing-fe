import { AUTHORIZED_ROUTES } from 'constants/routes';
import { Laptops, Users, Workspace } from 'pages';
import { Navigate, Route, Routes } from 'react-router-dom';

export const AuthorizedRoutes = () => {
  return (
    <Routes>
      <Route path={AUTHORIZED_ROUTES.workspace} element={<Workspace />} />
      <Route path={AUTHORIZED_ROUTES.users} element={<Users />} />
      <Route path={AUTHORIZED_ROUTES.laptops} element={<Laptops />} />
      <Route path="*" element={<Navigate to={AUTHORIZED_ROUTES.workspace} />} />
    </Routes>
  );
};
