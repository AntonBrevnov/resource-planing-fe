import { UNAUTHORIZED_ROUTES } from 'constants/routes';
import { SignIn } from 'pages';
import { Navigate, Route, Routes } from 'react-router-dom';

export const UnauthorizedRoutes = () => {
  return (
    <Routes>
      <Route path={UNAUTHORIZED_ROUTES.signIn} element={<SignIn />} />
      <Route path="*" element={<Navigate to={UNAUTHORIZED_ROUTES.signIn} />} />
    </Routes>
  );
};
