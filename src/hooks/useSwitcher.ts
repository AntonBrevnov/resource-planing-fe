import { useCallback, useMemo, useState } from 'react';
import { AnyEvent } from 'types';

export type UseSwitcherType = {
  isOn: boolean;
  on: () => void;
  off: () => void;
  toggle: () => void;
};

export const useSwitcher = (init = false) => {
  const [isOn, setIsOn] = useState(init);
  const on = useCallback((event?: AnyEvent) => {
    event?.stopPropagation();
    setIsOn(true);
  }, []);
  const off = useCallback((event?: AnyEvent) => {
    event?.stopPropagation();
    setIsOn(false);
  }, []);
  const toggle = useCallback((event?: AnyEvent) => {
    event?.stopPropagation();
    setIsOn((prev) => !prev);
  }, []);

  return useMemo(
    () => ({
      isOn,
      on,
      off,
      toggle,
    }),
    [isOn, off, on, toggle],
  );
};
