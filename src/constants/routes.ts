export const UNAUTHORIZED_ROUTES = {
  signIn: '/sign-in',
};

export const AUTHORIZED_ROUTES = {
  workspace: '/',
  users: '/users',
  laptops: '/laptops',
};
